<div class="card card-body mb-3">
	@if($project->status == 'En Proceso')
	<div class="text-white px-2 text-center bg-info">{{ $project->status }}	</div>
	@endif

	@if($project->status == 'Terminado')
	<div class="text-white px-2 text-center bg-success">{{ $project->status }}	</div>
	@endif

	@if($project->status == 'Atrasado')
	<div class="text-white px-2 text-center bg-warning">{{ $project->status }}	</div>
	@endif

	@if($project->status == 'Cancelado')
	<div class="text-white px-2 text-center bg-danger">{{ $project->status }}	</div>
	@endif


    <div class="card-body">	
	   <h5>{{ $project->name }}</h5>
	   <p>{{ $project->description }}</p>
     <h5>Usuarios</h5>
     @foreach($project->users as $user)
     <p>{{ $user->name }}</p>
     @endforeach
	   <hr>	
	   <a href="" data-toggle="modal" data-target="#modalCrearTarea_{{ $project->id }}" class="btn btn-outline-dark btn-sm mb-3">Crear tarea</a>

	     <a href="" data-toggle="modal" data-target="#modalUsuario_{{ $project->id }}" class="btn btn-outline-dark btn-sm mb-3">Agregar usuario</a>

	@foreach($project->tasks as $task)

<div class="d-flex algin-items-center justify-content-between">
	<div style="width:60%">
		<p class="mb-0">{{ $task->title }}</p>

    <p class="mb-0">Responsable: {{ $task->user->name }}</p>
		
		  @if($task->is_complete == false)
                                        <span class="badge badge-warning"> No disponible</span>
                                    @else
                                          <span class="badge badge-success">Completar</span>
                                    @endif
	</div>

<div>
	 
                                      </td>
                                      <td>
                                        <a href="{{ route('tareas.status',$task->id) }}" class="btn btn-outline-success btn-sm"><ion-icon name="checkbox-outline"></ion-icon></a>
                                    
                                     <a href="{{ route('tareas.edit',$task->id) }}" class="btn btn-outline-info btn-sm"><ion-icon name="create-outline"></ion-icon></a>

                                      <form method="POST" action="{{route('tareas.destroy', $task->id) }}">
                                      {{ csrf_field() }}
                                      {{ method_field('DELETE') }}

                                     <button type="submit" class="btn btn-danger btn-sm"><ion-icon name="trash-outline"></ion-icon></button>

                                 </form>
</div>
</div>



<p></p>
@endforeach

<hr>
<p>Creado el: {{ Carbon\Carbon::parse($project->created_at)->diffForHumans()}}</p>
<p>Creado el: {{ Carbon\Carbon::parse($project->created_at)->format('d M Y H:i') }}</p>
</div>
</div>
                                                                                                                              
<!-- Modal -->
<div class="modal fade" id="modalCrearTarea_{{ $project->id }}" tabindex="-1" role='dialog' aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Crear tarea</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="{{route('tareas.store')}}">
      <div class="modal-body"> 
      	{{ csrf_field() }}

         <input type="hidden" name="source"  value="proyectos">
        <input type="hidden" name="project_id"  value="{{ $project->id }}">

        <input type="hidden" name="user_id" value="{{ $project->id }}" readonly="">

							<div class="form-group">
								<label for="">Titulo tarea</label>
								<input type="text" name="title" class="form-control" required="">
							</div>

							<div class="form-group">
								<label for="">Fecha entrega</label>
								<input type="date" name="deadline" class="form-control">
							</div>

							<div class="form-group">
								<label for="">Descripcion</label>
								<textarea class="form-control" name="description" rows="5"></textarea>
							</div>


          <div class="form-group">
            <label for="exampleFormControlSelect1">Selecciona usuario</label>
            <select class="form-control" id="exampleFormControlSelect1" name="user_id">
              @foreach($project->users as $user)
              <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
            </select>
          </div>

					
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar tarea</button>
      </div>
      </form>	
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modalUsuario_{{ $project->id }}" tabindex="-1" role='dialog' aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agregar usuario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="{{route('tareas.store')}}">
      <div class="modal-body"> 
      	{{ csrf_field() }}

         <input type="hidden" name="source"  value="proyectos">
        <input type="hidden" name="user_id"  value="{{ $project->id }}">



<div class="form-check">
  @foreach($users as $user)
  <input class="form-check-input" type="checkbox" value="{{ $user->id }}" id="defaultCheck1" name="user_id[]">
  <label class="form-check-label" for="defaultCheck1">
    {{ $user->name }}
  </label>
  <br>
  @endforeach
</div>

					
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar </button>
        <button type="submit" class="btn btn-primary">Agregar usuario</button>
      </div>
      </form>	
    </div>
  </div>
</div>
